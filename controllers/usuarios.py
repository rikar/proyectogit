# -*- coding: utf-8 -*-
# Creado 06/05/2016 12:54 Por José Alfredo Pañola Madrigal

# Carga inicial
def index():
    #consulta selec(atributo1, atributo2)
    rows = db().select(db.usuarios.ALL,orderby=db.usuarios.nombre)
    return dict(rows=rows)

# Carga para la creacion de usuarios
def crear():
    if request.post_vars:
        #insertar
        db.usuarios.insert(nombre = request.post_vars.nombre,
                           amat = request.post_vars.amat,
                           apat = request.post_vars.apat,
                           edad = request.post_vars.edad,
                           sexo = request.post_vars.sexo
                          )
    return dict()

# Carga para la edicion de datos de usuarios
def editar():
    usuario_id = request.args(0)
    #row persona  = db.persona ( persona_id ) forma condensada
    row_usuarios  = db( db.usuarios.id == usuario_id).select(db.usuarios.ALL).first()
    #row persona  = db( db.persona.id == persona_id).select(db.persona.All).first()
    #                                   ^castear a int
    if request.post_vars:
        #actualizar
        db(db.usuarios.id==usuario_id).update(
            nombre=request.post_vars.nombre,
            amat = request.post_vars.amat,
            apat = request.post_vars.apat,
            edad = request.post_vars.edad,
            sexo = request.post_vars.sexo
        )
        redirect(URL('usuarios','index'))
    return dict(row_usuarios = row_usuarios)

# Carga para la eliminacion de usuarios
def eliminar():
    usuario_id = request.post_vars.persona_id
    db(db.usuarios.id==usuario_id).delete()
    return True
