# -*- coding: utf-8 -*-

# Carga Inicial del proceso
def index():
    response.flash = T("Hello World 9008")
    response.flash = T("Hola programadores de Datahome")
    response.flash = T("Hola programadores; seguimos cambiando el archivo")
    return dict(message=T('Welcome to web2py!'))

# Carga el usuario de autentificacion
def user():
    return dict(form=auth())


@cache.action()
def download():
    return response.download(request, db)


def call():
    return service()

def devuelveHolaMundoJMPC():
    return dict(msg="Hola Mundo")

"""
    Primer cambio de nuevo
"""

def saludar():
    saludo = "Bienvenidos al proyecto"
    return dict(saludo=saludo)
